#!/usr/bin/python
# -*- coding: utf-8 -*-
# Camille Watter
# BA thesis 2018 05 26

# Preprocessing (run script BEFORE using TrUDucer)
# Transform TIGER Dependency corpus from CONLL09 to CONLL-X format
# and prepare corpus for TrUDucer by storing each sentence in a different file in a directory




import re, codecs, sys, os


def check_for_obsolete_roots(current_sentence):
    '''
    check if sentence has more than one root
    '''
    root = []
    global danger
    danger = ''
    for line in current_sentence:
        if line[6] == '0':
            root.append(line[6])
    if len(root) >= 2:
        danger = 'yes'
    return danger



def change_root(current_sentence):
    '''
    first check for each root, how many dependent it has
    and make the root with most dependents the only root, 
    the other roots receive the UD dep dependency label
    '''
    collect_root = {}
    
    # collect all roots in one sentence, gather them in collect_root
    for line in current_sentence:
        if line[6] == '0' and line[0] not in collect_root:
            collect_root[line[0]] = 0
            

    # check for each root, how man dependent it has
    for line in current_sentence:
        if line[6] in collect_root.keys():
            collect_root[line[6]] += 1
            #print line[6]
    
    # find root with most dependents
    # if it has all equal, first in dict in picked (does not matter anyway in that case
    real_root = max(collect_root, key=collect_root.get)
    real_root = str(real_root)
    
    transformed_sentence = []
    
    for line in current_sentence:
        if line[6] == '0' and not line[0] == real_root:
            line[6] = real_root
            line[7] = 'DEP'
            #print line[7]
        transformed_sentence.append(line)
    return transformed_sentence





with codecs.open('TIGER_Dep_CoNLL09.txt', 'r', 'utf-8') as infile:
    
    single_file_counter = 1
    current_sentence = []
       
    for line in infile:
        
        # Delete Sentence Number at Beginning of Line
        line = re.sub('^[\d]+_', '', line)
        
        line = line.split()
        
        # act as sentence breaker
        if len(line) == 0:

            check_for_obsolete_roots(current_sentence)
            if danger == 'yes':
                change_root(current_sentence)

                
            #create new file for each sentence in directory tiger_singles_in
            file_number = str(single_file_counter) + '.txt'
            path = os.path.join('tiger_singles_in',file_number)
            outfile = open(path, 'w')            
            
            for indexed_line in current_sentence:
                 convert linelist in sentence list to string in unicode to write to outfile
                indexed_line = u'\t'.join(indexed_line).encode('utf-8')
                outfile.write(indexed_line)
                outfile.write('\n')
            #print current_sentence
            outfile.write('\n')
            outfile.close()  
           
            
            # create a fresh list for new sentence
            # and add 1 to sentence counter
            current_sentence = []
            single_file_counter += 1 
            print single_file_counter
            continue

        # delete obsolete tabs to get from CoNLL09 to CoNLL-X
        line.pop(5)
        line.pop(6)
        line.pop(7)
        line.pop()
        line.pop()

        # convert punctuation/root ('--' in tiger) for easy TrUDucer processing 
        if line[4] == '$.':
            line[7] = 'PUNCT'
        if line[4] == '$,':
            line[7] = 'PUNCT'
        if line[4] == '$(':
            line[7] = 'PUNCT'
        if line [6] == '0':
            line[7] = 'ROOT'
        #print line
        
        current_sentence.append(line)
