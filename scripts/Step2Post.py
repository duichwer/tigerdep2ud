#!/usr/bin/python
# -*- coding: utf-8 -*-
# Camille Watter
# BA thesis 2018 05 26

# Postprocessing (run script AFTER using TrUDucer)
# Transform the morphological features as well as the POS tags of the TIGER treebank to the Universal Dependencies format
# and join the sentences of the treebank back into one single file


import re, codecs, sys, os
from operator import itemgetter



def conv_morph(line):
    '''
    convert morph. feats to UD
    '''

    # split morph. feats to list
    morph = line[5].split('|')                                   

    # store changes of morph. features
    change_morph = []

    # Add PronTypes
    if line[4] == 'PDS' or line[4] == 'PDAT':
        change_morph.append('PronType=Dem')
    
    if line[4] == 'PIS' or line[4] == 'PIAT' or line[4] == 'PIDAT':
        change_morph.append('PronType=Ind')

    if line[4] == 'PWS' or line[4] == 'PWAT' or line[4] == 'PWAV':
        change_morph.append('PronType=Int')                

    if line[4] == 'PPOSS':
        change_morph.append('PronType=Prs')
        change_morph.append('Poss=Yes')
    if line[4] == 'PPOSAT':
        change_morph.append('PronType=Prs')
        change_morph.append('Poss=Yes')
                            
    if line[4] == 'PPER':
        change_morph.append('PronType=Prs')                                        
                    
    if line[4] == 'PRF':
        change_morph.append('PronType=Prs')  
        change_morph.append('Reflex=Yes')  


    if line[4] == 'PRELS' or line[4] == 'PRELAT':
        change_morph.append('PronType=Rel')
    
    
    #Add VerbForms
    if line[4] == 'VVFIN' or line[4] == 'VAFIN' or line[4] == 'VMFIN':
        change_morph.append('VerbForm=Fin')       
    
    if line[4] == 'VVINF' or line[4] == 'VAINF' or line[4] == 'VMINF':
        change_morph.append('VerbForm=Inf')    
    
    if line[4] == 'VVPP' or line[4] == 'VAPP' or line[4] == 'VMPP':
        change_morph.append('VerbForm=Part')  
    
    
    #Add Definite by checking lemma line[2]
    if line[4] == 'ART' and line[2] == 'der':
        change_morph.append('Definite=Def')
        change_morph.append('PronType=Art')
    if line[4] == 'ART' and line[2] == 'die':
        change_morph.append('Definite=Def')
        change_morph.append('PronType=Art')  
    if line[4] == 'ART' and line[2] == 'das':
        change_morph.append('Definite=Def')
        change_morph.append('PronType=Art')  
    if line[4] == 'ART' and line[2] == 'ein':
        change_morph.append('Definite=Ind')
        change_morph.append('PronType=Art')  
    if line[4] == 'ART' and line[2] == 'eine':
        change_morph.append('Definite=Ind')
        change_morph.append('PronType=Art')                                                                                                 
                                         
    #Add Foreign
    if line[4] == 'FM':
        change_morph.append('Foreign=Yes')   
    
    #Add Imperativ
    if line[4] == 'VVIMP' or line[4] == 'VAIMP':
        change_morph.append('Mood=Imp')
        
    #Add Polarity for "nicht" -> check lemma too
    if line[4] == 'PTKNEG' and line[3] == 'nicht':
        change_morph.append('Polarity=Neg')          
    #Add Polarity for "nein" / "ja" -> check lemma too
    if line[4] == 'PTKANT' and line[3] == 'nein':
        change_morph.append('Polarity=Neg') 
    if line[4] == 'PTKANT' and line[3] == 'ja':
        change_morph.append('Polarity=Pos')   
                                                                   
    for m in morph:
        m = m.replace("case","Case")
        m = m.replace("degree","Degree")
        m = m.replace("gender","Gender")
        m = m.replace("number","Number")
        m = m.replace("person","Person")
        m = m.replace("mood","Mood")
        m = m.replace("tense","Tense")
    
        m = m.replace("acc","Acc")
        m = m.replace("dat","Dat")
        m = m.replace("gen","Gen")
        m = m.replace("nom","Nom")
    
        m = m.replace("pl","Plur")
        m = m.replace("sg","Sing")
    
        m = m.replace("fem","Fem")
        m = m.replace("masc","Masc")
        m = m.replace("neut","Neut")
    
        m = m.replace("ind","Ind")
        m = m.replace("subj","Cnd")
    
        m = m.replace("comp","Cmp")
        m = m.replace("pos","Pos")
        m = m.replace("sup","Sup")
    
        m = m.replace("past","Past")
        m = m.replace("present","Pres")
        change_morph.append(m)
    
    # sort morph. features in alphabetical order
    morph = sorted(change_morph)
    
    # remove empty values '_', if it hase been replaced by a value
    if len(morph) >= 2 and '_' in morph:
        morph.remove('_')
    
    # convert list to string by '|' to get original form
    # and replace original by new morph in line
    morph = '|'.join(morph) 
    line[5] = morph

    return line



def adjust_KOKOM(line): 
    '''
    change KOKOM to either ADP or SCONJ
    '''
    if line[7] == 'case':
        line[3] = 'ADP' 
    if line[7] == 'mark':
        line[3] = 'SCONJ'
    return line   



def adjust_PWAV(line):
    '''
    change PWAV to either ADV or SCONJ
    '''
    if line[7] == 'mark':
        line[3] = 'SCONJ' 
    else:
        line[3] = 'ADV'
    return line



def adjust_aux(line):
    '''
    change auxiliary verb to VERB if it is not an auxiliary
    '''
    if line[7] == 'aux' or line[7] == 'aux:pass' or line[7] == 'cop':
        line[3] = 'AUX'
    else: 
        line[3] = 'VERB'
    return line



def adjust_modalverb(line):    
    '''
    change modal verbs to VERB if they are not auxiliaries
    '''
    if line[7] == 'aux' or line[7] == 'aux:pass' or line[7] == 'cop':
        line[3] = 'AUX'
    else:        
        line[3] = 'VERB'
    return line
        
        
        
def adjust_Compound_Prt(line):
    '''
    change separable verb prefix (PTKVZ) to either ADP or ADV
    source for preps: searched for all PTKVZ in TIGER treebank
    '''
    # [u'ab', u'abhanden', u'abw\xe4rts', u'an', u'auf', u'aufeinander', u'aufrecht', u'aus', u'auseinander', u'bei', u'beisammen', u'beiseite', u'bekannt', u'bereit', u'bescheid', u'bevor', u'blank', u'blo\xdf', u'brach', u'breit', u'da', u'dabei', u'dagegen', u'daher', u'dahin', u'dar', u'davon', u'dazu', u'dran', u'drauf', u'drin', u'durch', u'durcheinander', u'ein', u'einher', u'empor', u'entgegen', u'ernst', u'fehl', u'feil', u'fern', u'fertig', u'fest', u'fort', u'frei', u'gefangen', u'gegen', u'gegen\xfcber', u'gerade', u'gleich', u'heim', u'her', u'heran', u'herauf', u'heraus', u'herbei', u'herein', u'hernieder', u'herum', u'herunter', u'hervor', u'hin', u'hinab', u'hinauf', u'hinaus', u'hinein', u'hinterher', u'hinunter', u'hinweg', u'hinzu', u'hoch', u'h\xe4ngen', u'inne', u'kalt', u'kaputt', u'kennen', u'klar', u'kund', u'kurz', u'lahm', u'leer', u'leicht', u'liegen', u'locker', u'los', u'matt', u'mit', u'mi\xdf', u'nach', u'nahe', u'nieder', u'not', u'offen', u'parat', u'platt', u'pleite', u'preis', u'rauf', u'raus', u'rein', u'runter', u'sauber', u'schief', u'schwer', u'sch\xe4tzen', u'sicher', u'sitzen', u'stand', u'stark', u'statt', u'stehen', u'still', u'stramm', u'streitig', u'teil', u'um', u'umher', u'umhin', u'unter', u'ver', u'verantwortlich', u'verloren', u'vor', u'voran', u'voraus', u'vorbei', u'vorw\xe4rts', u'vor\xfcber', u'wach', u'wahr', u'weg', u'weh', u'weiter', u'wett', u'wider', u'wieder', u'zu', u'zuende', u'zugrunde', u'zugute', u'zunichte', u'zurecht', u'zur\xfcck', u'zusammen', u'zustande', u'zutage', u'zuteil', u'zuvor', u'\xfcbel', u'\xfcber', u'\xfcberein', u'\xfcbrig']
    preps = ['ab', 'an', 'auf', 'aus', 'bei', 'durch', 'entgegen', 'gegen', 'gegenüber', 'mit', 'nach', 'nahe', 'um', 'unter', 'vor', 'voran', 'voraus', 'zu', 'über', 'überein', 'übrig']
    
    if line[2] in preps:
        line[3] = 'ADP'
    else:
        line[3] = 'ADV'
    return line        



def conv_POS(line):
    '''
    change POS to UPOS
    '''
    
    if line[4] == 'ADJA':
        line[3] = 'ADJ'

    elif line[4] == 'APPO' or line[4] == 'APPRART' or line[4] == 'APPR' or line[4] == 'APZR':
        line[3] = 'ADP'

    # als/ wie bei Tiger 'CM' ändern UPOS je nach deprel
    elif line[4] == 'KOKOM':
        adjust_KOKOM(line)     

    elif line[4] == 'ADV' or line[4] == 'PROAV' or line[4] == 'ADJD':
        line[3] = 'ADV'
    elif line[4] == 'PWAV':
        adjust_PWAV(line)   
      
    elif line[4] == 'KON':
        line[3] = 'CCONJ'
    
    elif line[4] == 'ART' or line[4] == 'PDAT' or line[4] == 'PIAT' or line[4] == 'PIDAT' or line[4] == 'PPOSAT' or line[4] == 'PRELAT' or line[4] == 'PWAT':
        line[3] = 'DET'

    elif line[4] == 'ITJ':
        line[3] = 'INTJ'
        
    elif line[4] == 'NN':
        line[3] = 'NOUN'

    elif line[4] == 'CARD':
        line[3] = 'NUM'

    elif line[4] == 'PTKA' or line[4] == 'PTKANT' or line[4] == 'PTKNEG' or line[4] == 'PTKZU':
        line[3] = 'PART'
                
    elif line[4] == 'PDS' or line[4] == 'PIS' or line[4] == 'PPER' or line[4] == 'PPOSS' or line[4] == 'PRELS' or line[4] == 'PRF' or line[4] == 'PWS':
        line[3] = 'PRON'

    elif line[4] == 'NE' or line[4] == 'NNE':
        line[3] = 'PROPN'
    
    elif line[4] == 'PUNCT' or line[4] == '$.' or line[4] == '$,' or line[4] == '$(':
        line[3] = 'PUNCT'        

    elif line[4] == 'KOUI' or line[4] == 'KOUS':
        line[3] = 'SCONJ'
        
#    elif line[4] == '':
#        line[4] = 'SYM'
 
                
    elif line[4] == 'VAFIN' or line[4] == 'VAIMP' or line[4] == 'VAINF' or line[4] == 'VAPP':# or line[4] == 'VMFIN' or line[4] == 'VMINF'  or line[4] == 'VMPP':
        if line[2] == 'sein':
            adjust_aux(line)
        if line[2] == 'werden':
            adjust_aux(line)    
        if line[2] == 'haben':
            adjust_aux(line)
        else:            
            line[3] = 'AUX'

    elif line[4] == 'VVFIN' or line[4] == 'VVIMP' or line[4] == 'VVINF' or line[4] == 'VVIZU' or line[4] == 'VVPP':
        line[3] = 'VERB'

    elif line[4] == 'FM' or line[4] == 'TRUNC' or line[4] == 'XY':
        line[3] = 'X'

    
    # Modal verbs as VERB
    elif line[4] == 'VMINF' or line[4] == 'VMFIN' or line[4] == 'VMPP':
        adjust_modalverb(line)        
    
     #  separable verb prefix distinguish to ADP/ADV
    elif line[4] == 'PTKVZ':
        adjust_Compound_Prt(line)
    
    else: 
        print line[4]

    return line



path = 'tiger_singles_out/'
file_list = []
for files in os.listdir(path):
    file_list.append(files)

nr_of_files = len(file_list)





with codecs.open('TIGER_UD_CoNLLU.txt','w') as outfile:
    counter = 1
    for i in range(1,nr_of_files+1):
    
        i = str(i) + '.txt'
        path = os.path.join('tiger_singles_out/',i)
    
        with codecs.open(path,'r') as infile:
            
            current_sentence = []
             
            for line in infile:
                              
                line = line.split()

                
                if len(line) != 0 and line[0] != '#': 
                    
                    x = int(line[0])
                    line.pop(0)
                    line.insert(0,x)
                    
                    # convert morph. features 
                    conv_morph(line)
                
                    # convert POS to UPOS
                    conv_POS(line)
                
                    current_sentence.append(line)
            
            
            current_sentence = sorted(current_sentence)
            
            for line in current_sentence:
                x = str(line[0])
                line.pop(0)
                line.insert(0,x)
                
                #print line    
                
                line = '\t'.join(line) 
                outfile.write(line)
                outfile.write('\n')
            outfile.write('\n')
            print counter
            counter += 1
            
            
              