# Converting the TIGER Dependency Treebank to the Universal Dependency Format using the TrUDucer

Camille Watter, BA Thesis 2018/05/26

### Installing the software (on MacOS)
 - If you haven’t already, install java and maven to run the TrUDucer as a java jar file.
 - java: [https://www.java.com/de/download/](https://www.java.com/de/download/)

 - How to install maven on mac: [https://www.youtube.com/watch?v=EoXImdzlAls](https://www.youtube.com/watch?v=EoXImdzlAls)

 - How to run a jar file with maven: [https://www.youtube.com/watch?v=vGtGxKZQ-l8](https://www.youtube.com/watch?v=vGtGxKZQ-l8)

 - Download the TrUDucer by Felix Hennig: [https://gitlab.com/nats/TrUDucer](https://gitlab.com/nats/TrUDucer)

 - accompanying paper: [http://edoc.sub.uni-hamburg.de/informatik/volltexte/2017/230/](http://edoc.sub.uni-hamburg.de/informatik/volltexte/2017/230/) and accompanying website: [https://nats.gitlab.io/truducer/](https://nats.gitlab.io/truducer/) 

### Corpus Processing
 - Download the TIGER Dependency corpus in the CONLL09 format: [http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/download/start.html](http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/download/start.html) (or use the corpus in the repository).

- Run the Python script „Step1Prep.py“ on the TIGER corpus that converts the corpus from the CONLL09 to the CONLL-X format and stores each sentence of the treebank in a separate file in the directory „tiger_singles_in“.

- The TIGER treebank is now ready for processing by the TrUDucer that transforms the dependency labels to the Universal Dependencies format using the 130 rules of the rule file „RuleSet_Tiger“. Each sentence is computed individually and stored in the directory „tiger_singles_out“. Run the TrUDucer with the following command in your terminal:

java -jar TrUDucer.jar convall RuleSet_TIGER.tud tiger_singles_in/ tiger_singles_out

The TrUDucer offers additional processing features that can be applied by the following commands: ([https://gitlab.com/nats/TrUDucer/blob/master/README.md](https://gitlab.com/nats/TrUDucer/blob/master/README.md)) 

```
conv
  - convert one single sentence to UD
  - java -jar TrUDucer.jar conv rulefile infile outfile

convall
  - convert a whole directory of files
  - java -jar TrUDucer.jar convall rulefile input_dir output_dir

compare
  - compare two conll files to test how well the transducer has done
  - java -jar TrUDucer.jar compare expected_dir actual_dir original_dir

coverage
  - check how many dependency relations are converted
  - java -jar TrUDucer.jar coverage dir original_dir

show
  - show the conversion process of a single tree step by step in GUI
  - java -jar TrUDucer.jar show input_file [rule_file]
```
 - Finally, run the Python script „scripts/Step2Post.py“ that transforms the morphological features as well as POS tags to the UD format. Also, all separate sentence files of the TIGER treebank are joined back into one single file. Voilà, you now have the TIGER treebank in the Universal Dependencies format.
