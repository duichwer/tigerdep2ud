# Camille Watter, 2018 05 26
# This rule set of 130 rules was developed to convert the dependency labels of the TIGER Dependency treebank to the Universal Dependencies format during by bachelor thesis for the Institute of Computational Linguistics at the University of Zurich.


expansion verbPos = [VVFIN,VVIMP,VVINF,VVIZU,VVPP,VAFIN,VAIMP,VAINF,VAPP,VMFIN,VMINF,VMPP];

# obl for parents that are VV, ADJ, ADV
expansion oblPos = [VVFIN,VVIMP,VVINF,VVIZU,VVPP,VAFIN,VAIMP,VAINF,VAPP,VMFIN,VMINF,VMPP, ADJA, ADJD, ADV, PROAV];
expansion infPos = [VVINF,VAINF,VMINF];
expansion passPos = [VVPP,VAPP,VMPP];
expansion attrPronPos = [PDAT, PIAT, PIDAT, PWAT, PRELAT];
expansion prepPos = [APPO,APPR,APPRART];
expansion advPos = [ADJD, ADV];
expansion nnePos = [NN,NE];

###
### CHANGE ROOT : PD -> root; HD -> cop
###
parent:$x({n:PD(?pdr), ?fr}, ?r) -> n:$x(parent:cop(), ?pdr, ?r, ?fr);


###
### fixed word expression
###
# nach wie vor -> advmod, fixed, fixed
pp:MO(p:CD(n:CJ())) -> pp:advmod(p:fixed(), n:fixed()) :- {pp.getLemma() == "nach" & p.getLemma() == "wie" && n.getLemma() == "vor"};

# Um so mehr -> advmod, fixed, fixed
pp:MO(p:MO(n:AVC())) -> n:advmod(p:fixed(),pp:fixed()) :- {pp.getLemma() == "mehr" & p.getLemma() == "so" && n.getLemma() == "um"};

# Herr/Frau Müller -> change dep
p:$x({n.NE:NK(?r),?r1}, ?r2) -> n:$x(p:compound(), ?r, ?r1, ?r2) :- {p.getLemma() == "Herr" || p.getLemma() == "Frau"}; 

###
#### CASE : change HEAD
###
parent.prepPos:$x({n:NK(?r1),?r},?r2) -> n:$x(parent:case(), ?r,?r1,?r2); 
n.APZR:$x() -> n:case();
n:AC() -> n:case();

### ADV : PROAV
n.PROAV:CVC() -> n:advmod();
n.PROAV:JU() -> n:advmod();
n.PROAV:MNR() -> n:advmod();
n.PROAV:MO() -> n:advmod();
n.PROAV:NK() -> n:advmod();
n.PROAV:OC() -> n:advmod();
n.PROAV:PG() -> n:advmod();

### ADV: PWAV
n.PWAV:CVC() -> n:mark();
n.PWAV:NK() -> n:mark();
n.PWAV:OP() -> n:mark();
p:root({n.PWAV:MO()}) -> p(n:advmod());
p.verbPos:$x({n.PWAV:MO()}) -> p:$x(n:mark());
n.PWAV:MO() -> n:advmod();


###
#### OC
###
# 1) PASSIV: dep change, if werden + passPos(=VVPP/VAPP/VMPP)
parent.verbPos:$x({n.passPos:OC(?ocr), n1:SB(), ?fr}, ?r) -> n:$x(parent:aux:pass(), n1:nsubj:pass(), ?ocr, ?r, ?fr) :- {parent.getLemma() == "werden"};      #passive, wenn werden vorkommt + SB -> nsubjpass
parent.verbPos:$x({n.passPos:OC(?ocr), n1:SB(), n2:aux(), ?fr}, ?r) -> n:$x(parent:aux:pass(), n1:nsubj:pass(), n2(), ?ocr, ?r, ?fr) :- {parent.getLemma() == "werden"};      #passive, wenn werden vorkommt + SB -> nsubjpass
parent.verbPos:$x({n.passPos:OC(?ocr), ?fr}, ?r) -> n:$x(parent:aux:pass(), ?ocr, ?r, ?fr) :- {parent.getLemma() == "werden"};       #passive, wenn kein SB vorkommt + erkannt worden war --> weiter verschieben 

# 2) xcomp with wordlist
parent.verbPos:$x({n:OC(?ocr), ?fr}, ?r) -> parent:$x(n:xcomp(?ocr), ?r, ?fr) :- {parent.getLemma() == "abraten" || parent.getLemma() == "anlasten"  || parent.getLemma() == "ankreiden" || parent.getLemma() == "aufzwingen" || parent.getLemma() == "bezichtigen" || parent.getLemma() == "beschuldigen" || parent.getLemma() == "drängen" || parent.getLemma() == "empfehlen" || parent.getLemma() == "erlauben" || parent.getLemma() == "gelingen" || parent.getLemma() == "helfen" || parent.getLemma() == "hindern" || parent.getLemma() == "kritisieren" || parent.getLemma() == "konfrontieren" || parent.getLemma() == "mahnen" || parent.getLemma() == "überreden" || parent.getLemma() == "überzeugen" || parent.getLemma() == "untersagen" || parent.getLemma() == "verpflichten" || parent.getLemma() == "verurteilen" || parent.getLemma() == "vorwerfen" || parent.getLemma() == "verbieten" || parent.getLemma() == "zumuten" || parent.getLemma() == "zwingen" || parent.getLemma() == "abhalten" || parent.getLemma() == "ablehnen" || parent.getLemma() == "ableugnen" || parent.getLemma() == "abstreiten" || parent.getLemma() == "akzeptieren" || parent.getLemma() == "aufgeben" || parent.getLemma() == "ausschliessen" || parent.getLemma() == "ausschließen" || parent.getLemma() == "aufzwingen" || parent.getLemma() == "berechtigen" || parent.getLemma() == "bedauern" || parent.getLemma() == "befürchten" || parent.getLemma() == "bereuen" || parent.getLemma() == "blockieren" || parent.getLemma() == "drohen" || parent.getLemma() == "freuen" || parent.getLemma() == "hoffen" || parent.getLemma() == "versprechen" || parent.getLemma() == "verpassen" || parent.getLemma() == "verpaßen" || parent.getLemma() == "versäumen" || parent.getLemma() == "verabscheuen" || parent.getLemma() == "vorhaben" || parent.getLemma() == "vorgeben" || parent.getLemma() == "weigern" || parent.getLemma() == "scheitern" || parent.getLemma() == "verzichten" || parent.getLemma() == "zustimmen" || parent.getLemma() == "zugeben" || parent.getLemma() == "zurückschrecken"};

# 3) dep change if fin. VA/VM or (modal) INF
parent.VAFIN:$x({n:OC(?ocr), ?fr}, ?r) -> n:$x(parent:aux(), ?ocr, ?r, ?fr);
parent.VMFIN:$x({n:OC(?ocr), ?fr}, ?r) -> n:$x(parent:aux(), ?ocr, ?r, ?fr);
parent.infPos:$x({n:OC(?ocr), ?fr}, ?r) -> n:$x(parent:aux(), ?ocr, ?r, ?fr);

# 4 A) OC -> advcl with SB and CP/MO(mark)
p.oblPos({n:OC(m:CP(),o:SB(),?r)}) -> p(n:advcl(m:mark(),o:nsubj(),?r));    
# 4 B) acl if p.NN / p.NE
p.nnePos({n:OC()}) -> p(n:acl());

# 5) catch-all OC
n:OC() -> n:ccomp();


###
### CONJUNCT (cc + conj)
###
p(n1:CJ(n2:CJ())) -> p(n1(),n2()); # lift up
p(n1:CJ(n2:CD())) -> p(n1(),n2()); # lift up
p:$x({n1:CD(n2:CJ(?r2), ?r1)},?r3) -> p:$x(n2:conj(n1:cc(), ?r2, ?r1)); 
n:CD() -> n:cc();
n:CJ() -> n:conj();


###
### OBJECTS
###
n:OA() -> n:obj();
n:OA2() -> n:obj();
n:DA() -> n:iobj();
n:OG() -> n:obl:gen();   

# OP -> obl:arg     Prepositional object
p({n.NE:OP()}) -> p(n:appos());
p.NN({n:OP()}) -> p(n:nmod());      
p({n.CARD:OP()}) -> p(n:parataxis());
n:OP() -> n:obl:arg();


### UC Unit component
n.FM:UC() -> n:flat:foreign(); 
n.FM:NK() -> n:flat:foreign();
n.KOKOM:UC() -> n:case();    
n:UC() -> n:flat();   


###
### ADJECTIVES
###
p.NN({n1.ADJA:NK(n2.NE:ADC())}) -> p(n2:amod(n1:flat()));  
n.TRUNC:ADC() -> n:flat(); 
n.ADJA:NK() -> n:amod(); 
n.ADJA:MO() -> n:amod(); 


###
### APPOSITIONS / parataxis
###
p:$x({n.verbPos:APP()}) -> p:$x(n:parataxis()); 

p:$x({n:APP()}) -> p:$x(n:appos());     


###
### CM / CC -> case, nmod/obl [als --] Vergleichsergänzung
###
expansion ccNumPos = [CARD,ART];

p.oblPos:$x({n.verbPos:CC()}) -> p:$x(n:advcl()); 
p:$x({n.verbPos:CC()}) -> p:$x(n:acl());

p.oblPos:$x({n:CC()}) -> p:$x(n:obl());        

# same construction but parent = nominal -> nmod (usual case)
n.ccNumPos:CC() -> n:nummod();    
p.verbPos:$x({n:CM()}) -> p:$x(n:mark());
n:CM() -> n:case();
n:CC() -> n:nmod(); 

###
### NUMERAL MODIFIER
###
p:$x({n1.CARD:NMC(), n2.NN:NK(),?r},?r1) -> n2:$x(p:nummod(n1:flat(),?r,?r1)); #6 Million Dollar
p:$x({n1.ART:NMC(), n3.ADJA:NMC(), n2.NN:NK(),?r},?r1) -> n2:$x(p:nummod(n1:flat(),n3:flat(),?r,?r1)); #eine halbe Million Dollar
p:$x({n1.ART:NMC(), n2.NN:NK(),?r},?r1) -> n2:$x(p:nummod(n1:flat(),?r,?r1)); #eine Million Dollar
p.oblPos({n:AMS()}) -> p(n:obl());
n.CARD:AMS() -> n:nummod();
n:AMS() -> n:nmod();

n:NMC() -> n:nummod();
n.CARD:NK() -> n:nummod();


###
### MODIFIER : nmod vs. obl
###
expansion nomMod = [MNR, CVC];

n.advPos:MO() -> n:advmod();

n.ADV:MNR() -> n:advmod();
n.KOUS:MO() -> n:mark();
n.PTKA:MO() -> n:mark();

### NS bei Modifier
p({n.verbPos:MNR()}) -> p(n:acl());

p.oblPos({n.verbPos:MO()}) -> p(n:advcl());
p({n.verbPos:MO()}) -> p(n:acl());

p.oblPos({n:nomMod()}) -> p(n:obl());  

p.oblPos({n:MO()}) -> p(n:obl());      

n:nomMod() -> n:nmod();         

n:MO() -> n:nmod();              

p.NN({n.NN:NK()}) -> p(n:compound());



###
### PERSONAL NAMES
###
# link NE to NN:NK via appos
p.NN({n1.NE:NK()}) -> p(n1:appos());

n.CARD:PNC() -> n:nummod(); 
n.ADJA:PNC() -> n:flat();   
n.ADJD:PNC() -> n:flat();   
n.ADV:PNC() -> n:flat();    
n.KOUS:PNC() -> n:flat();   

# link NE to NN:PNC 
p.NE:$x({n:PNC(?r),?r1}, ?r2) -> n:$x(p:flat:name(), ?r, ?r1, ?r2);

# link NN to NN                 
p.NN:$x({n.NN:PNC(?r),?r1}, ?r2) -> n:$x(p:compound(), ?r, ?r1, ?r2);

# E dep change if foreign
p:$x({n.FM:PNC(?r),?r1},?r2) -> n:$x(p:flat:foreign(), ?r, ?r1, ?r2);

# change parent with PNC and convert PNC -> 
p:$x({n:PNC(?r),?r1},?r2) -> n:$x(p:flat(), ?r, ?r1, ?r2);


### Special Rules for SP
# sentence 1 " was wir jetzt brauchen, sind keien Politiker"
p:$x({n1:RS(n2.NN:SP(?r2),?r1,n3.VVFIN:SP(?r3))}) -> p(n2:parataxis(n1:aux(),?r1,?r2,n3:csubj(?r3)));

# sentence 2 "Darin heisst es unter anderem: Dies wird jetzt eine Zeit sein für natürlichen und verständlichen Schmerz.
p:$x({n.NN:SP(),?r1},?r) -> n:$x(p:aux(),?r,?r1);
n:SP() -> n:nsubj();

###
### NEBENSAETZE
###
n:RC() -> n:acl:relcl();

n.verbPos:SB() -> n:csubj();

n.verbPos:NK() -> n:parataxis();

p.oblPos({n:RE()}) -> p(n:advcl());
n:RE() -> n:acl();


###
### PARATAXIS
###
n:RS() -> n:parataxis();
n.PPER:PAR() -> n:nsubj();
n:PAR() -> n:parataxis();


###
### INTERJECTION, discourse
###
expansion interPos = [DM, MO, NK, VO];
n.ITJ:interPos() -> n:discourse();   

n:DM() -> n:discourse(); # Antwortpartikel
n.PTKANT:MO() -> n:discourse(); 

###
### ADVERBS
###
n1:MO(n2:AVC()) -> n2:advmod(n1:fixed());   
n:AVC() -> n:advmod();                      

n:NG() -> n:advmod();


###
### simple one-to-one mappings
###
n.PPOSAT:NK() -> n:det:poss();
n.attrPronPos:NK() -> n:det(); 
n.ART:NK() -> n:det();  

expansion epPos = [EP,PH];
n:epPos() -> n:expl();          

p({n1:SB(), n2:SBP()}) -> p(n1:nsubj:pass(), n2:obl:agent());

n:SB() -> n:nsubj();            
n:SBP() -> n:obl:agent();

n:DEP() -> n:dep();

n:CP() -> n:mark();             
n.FM:NK() -> n:flat:foreign(); 
n:VO(?r) -> n:vocative(?r);     
n:JU() -> n:cc(); 

p.oblPos:$x({n:PG()}) -> p(n:obl());      
n:AG() -> n:nmod();
n:PG() -> n:nmod();

n:PM() -> n:mark();
n:PUNCT() -> n:punct();
n:ROOT() -> n:root(); 
n:SVP() -> n:compound:prt();     

# catch-all NK
expansion apposPOS = [NN, TRUNC]; 
p.oblPos:$x({n:NK()}) -> p(n:obl());
p.NN:$x({n.apposPOS:NK()}) -> p(n:appos());
n:NK() -> n:nmod();